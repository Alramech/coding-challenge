Run as 
python vacationing-salesman.py [miles/kilo] 

I chose python because it was a short coding challenge, so 
a scripting language seemed more appropriate. I also did not need
to do any low level work.

The primary design decision was how to find the distance between two cities.
Since no points were given, I assumed that the cities could be any possible city,
so tried to find an API to give distances or GPS coordinates. Failing to find a 
comprehensive one, I gave up and continued writing with an assumption that 
I had a dictionary that had all the requisite entries., in GP

I ran out of time in figuring out an actual API to get distances from
the city names. I ended up assuminmg that there is a dictionary that maps city names to points
on a map, and coded from there.
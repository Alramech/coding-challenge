I decided on a basic implementation where the deck is a list of cards.
This was chosen for ease of implementation.
Shuffling is done by using numpy functions to create an array of random indexes, and use that to 
shuffle the deck.

There was an amibiguity in whether signalling an error meant throwing an error or just indicating
that there was an error, so I just printed out that the deck was empty. 

Testing:
  Inititallize a new deck, attempt to draw 53 cards out of it, check if there is an error
  Shuffle, check if can pull 52 cards out without error
  Shuffle, check if order of deck is different.
  Pull 52 cards out of the deck, check if each card is different
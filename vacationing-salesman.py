import sys
from math import radians, cos, sin, asin, sqrt

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 3956 # Radius of earth in miles
    return c * r


def travelAll(cities):
    """
    Takes in a list of strings, and outputs the
    pairwise distance between each city
    """
    start = 0
    next = 1
    out = []
    while next < len(cities):
        out.append(travel(cities[start], cities[next]))
        start += 1
        next += 1
    return out

def travel(c1, c2):
    """
    Takes 2 cities as input, and returns the distance between the two
    """    
    lat1, lon1 = dict[c1]
    lat2, lon2 = dict[c2]
    return haversine(lon1, lat1, lon2, lat2)
    
def printResults(tlist, dlst, measure):
    start = 0
    next = 1
    while next < len(tlist):
        print("{} -> {}: {} {}".format(tlist[start], tlist[next], dlist[start], measure)
        start += 1
        next += 1
      print("Total Distance covered: {} {}".format(sum(dlist), measure) 
    
if __name__ == "__main__":
     
    args = sys.argc[1:]
    
    tList = []
    for line in sys.stdin:
        tList.append(line)
    dist = travelAll(tList)
    if args[0] = "kilo":
        map(lambda x:1.606*x. dist)
    printResults(tlist, dist)
        
    
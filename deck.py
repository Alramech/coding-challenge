import random
import numpy as np

class Deck:
    
    """
    A deck of cards. The cards are represented by a tuple of 
    either H S D C, representing the suit, and an integer,
    representing the numbers. Aces are 1, jacks are 11, 
    queens are 12, and kings are 13.
    """
    def __init__(self):
        self.deck = self.addAllCards()
    
    """
    Initializes the deck in sorted order.
    """
    def addAllCards(self):
        deck = []
        suits = ["H", "D", "C", "S"]
        values = range(1, 14)
        for suit in suits:
            for value in values:
                deck.append([suit, value])
        return deck
        
    """
    Shuffles the cards in the deck.
    """
    def Shuffle(self):
        deck = np.array(self.addAllCards())
        indii = np.random.choice(52, 52, replace=False)
        self.deck = deck[indii].tolist()
    
    """
    Returns the top card of the deck
    """
    def GetNextCard(self):
        try:
            top = self.deck.pop()
            return top
        except IndexError:
            print("Deck is empty")
        